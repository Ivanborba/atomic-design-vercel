import { Container } from "../../../globalStyles";
import Input from "../../atoms/input";
import Button from "../../atoms/button";

const data = ["Name", "Password"];

const LoginForm = () => {
  return (
    <Container>
      {data.map((actual) => {
        return <Input label={actual} />;
      })}
      <Button />
    </Container>
  );
};

export default LoginForm;
