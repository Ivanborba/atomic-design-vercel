import Link from "../../atoms/link";
import "./index.css";

const links = ["About Us", "Contact"];

const Footer = () => {
  return (
    <div className="footer">
      {links.map((actual) => {
        return <Link text={actual} />;
      })}
    </div>
  );
};

export default Footer;
