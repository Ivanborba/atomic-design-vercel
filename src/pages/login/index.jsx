import LoginForm from "../../components/molecules/login-form";

const Login = () => {
  return (
    <div>
      <h1 style={{ marginTop: "8%", marginLeft: "40%" }}>Login</h1>
      <LoginForm />
    </div>
  );
};

export default Login;
