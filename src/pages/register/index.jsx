import RegisterForm from "../../components/molecules/register-form";

const Register = () => {
  return (
    <div>
      <h1 style={{ marginTop: "8%", marginLeft: "40%" }}>Register</h1>
      <RegisterForm />
    </div>
  );
};

export default Register;
